import React from "react";
import Header from "./components/header/header.component.jsx";
import LogList from "./components/log-list/log-list.component.jsx";

import "./App.scss";

const App = () => (
  <div className="App">
    <Header />
    <LogList />
  </div>
);

export default App;

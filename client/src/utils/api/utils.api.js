export const handleErrors = response => {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
};

export const getLogData = async () => {
  try {
    const res = await fetch("/api/eye-log").then(handleErrors);
    const data = await res.json();
    return data;
  } catch (error) {
    console.error("Could not fetch log data: ", error);
    return null;
  }
};

export const updateLogItem = async item => {
  try {
    const res = await fetch("/api/eye-log", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(item)
    }).then(handleErrors);
    const data = await res.json();
    return data;
  } catch (error) {
    console.error("Could not update item: ", error);
    return null;
  }
};

export const createLogItem = async item => {
  try {
    const res = await fetch("/api/eye-log", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(item)
    }).then(handleErrors);
    const data = await res.json();
    return data;
  } catch (error) {
    console.error("Could not create item: ", error);
    return null;
  }
};

export const deleteLogItem = async item => {
  try {
    const res = await fetch("/api/eye-log", {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ id: item.id })
    }).then(handleErrors);
    const data = await res.json();
    return data;
  } catch (error) {
    console.error("Could not delete item: ", error);
    return null;
  }
};

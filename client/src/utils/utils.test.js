import { timestampToDateTime } from "./utils.js";

describe("Utils", () => {
  let mockTimestamp = 1585725735364;
  it("should correctly convert timestamp to datetime string", () => {
    expect(timestampToDateTime(mockTimestamp)).toEqual(
      "April 1, 2020, 10:22 AM"
    );
  });
});

import React from "react";
import "./log-list-item.styles.scss";
import { timestampToDateTime } from "../../utils/utils.js";

const LogListItem = ({ logItem, handleItemSelect }) => {
  const { timestamp, rightEye, leftEye, condition, note, id } = logItem;
  return (
    <div className="log-item">
      <ul onClick={() => handleItemSelect(logItem)}>
        <li className="id">#{id}</li>
        <li className="timestamp">{timestampToDateTime(timestamp)}</li>
        <li className="left-eye">L {leftEye} cm</li>
        <li className="right-eye">R {rightEye} cm</li>
        <li className="condition">{condition}</li>
        <li className="note">{note}</li>
      </ul>
    </div>
  );
};

export default LogListItem;

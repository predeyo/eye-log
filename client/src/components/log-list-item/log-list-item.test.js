import React from "react";
import { shallow } from "enzyme";
import LogListItem from "./log-list-item.component.jsx";
import { timestampToDateTime } from "../../utils/utils.js";

describe("LogListItem component", () => {
  let wrapper;
  let mockLogItem = {
    id: 200,
    timestamp: 1585725735364,
    leftEye: 212,
    rightEye: 213,
    condition: "rested",
    note: "Lorem"
  };
  let mockHandleItemSelect = jest.fn();
  let mockProps;

  beforeEach(() => {
    mockProps = {
      logItem: mockLogItem,
      handleItemSelect: mockHandleItemSelect
    };
    wrapper = shallow(<LogListItem {...mockProps} />);
  });

  it("should render LogListItem component", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
  it("should render correct ID", () => {
    expect(wrapper.find(".id").text()).toEqual("#" + mockLogItem.id);
  });
  it("should render correct datetime", () => {
    expect(wrapper.find(".timestamp").text()).toEqual(
      timestampToDateTime(mockLogItem.timestamp)
    );
  });
  it("should render correct datetime", () => {
    expect(wrapper.find(".timestamp").text()).toEqual(
      timestampToDateTime(mockLogItem.timestamp)
    );
  });
  it("should render correct left eye value", () => {
    expect(wrapper.find(".left-eye").text()).toEqual(
      "L " + mockLogItem.leftEye + " cm"
    );
  });
  it("should render correct right eye value", () => {
    expect(wrapper.find(".right-eye").text()).toEqual(
      "R " + mockLogItem.rightEye + " cm"
    );
  });
  it("should render correct condition value", () => {
    expect(wrapper.find(".condition").text()).toEqual(mockLogItem.condition);
  });
  it("should render correct note", () => {
    expect(wrapper.find(".note").text()).toEqual(mockLogItem.note);
  });
  it("should call handleItemSelect when item is clicked", () => {
    wrapper.find("ul").simulate("click");
    expect(mockHandleItemSelect).toBeCalled();
  });
});

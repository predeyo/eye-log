import React from "react";
import { shallow } from "enzyme";
import Modal from "./modal.component.jsx";

describe("Modal component", () => {
  let wrapper;
  let mockClosed = false;
  let mockToggleClosed = jest.fn();
  let mockChildren = "Children";
  let mockProps;

  beforeEach(() => {
    mockProps = {
      closed: mockClosed,
      toggleClosed: mockToggleClosed,
      children: mockChildren
    };
    wrapper = shallow(<Modal {...mockProps} />);
  });

  it("should render Modal component", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
  it("should render correct children", () => {
    expect(wrapper.find(".modal").text()).toEqual(mockChildren);
  });
});

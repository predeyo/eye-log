import React, { useEffect, useRef } from "react";
import "./modal.styles.scss";

const Modal = ({ closed, toggleClosed, children, offClick = true }) => {
  const modalRef = useRef();

  useEffect(() => {
    const modal = modalRef.current;
    modal.classList.add("fade-in");
    // Focus current modal
    modalRef.current.focus();
    // Focus previous modal
    return () => {
      modal.classList.add("fade-out");
      // Currently open modals including this modal
      const openModals = document.getElementsByClassName("modal-container");
      const openModalCount = openModals.length;
      if (openModalCount > 1) openModals[openModalCount - 2].focus();
    };
  }, []);

  const handleEscape = e => {
    return e.key === "Escape" ? handleClose() : null;
  };

  const transitionEnd = e => {
    if (
      e.propertyName !== "opacity" ||
      modalRef.current.classList.contains("fade-in")
    ) {
      return;
    }
    if (modalRef.current.classList.contains("fade-out")) {
      toggleClosed();
    }
  };

  const handleClose = e => {
    modalRef.current.classList.add("fade-out");
    modalRef.current.classList.remove("fade-in");
  };
  return (
    <div
      className="modal-container"
      onKeyDown={handleEscape}
      tabIndex={0}
      ref={modalRef}
      onTransitionEnd={transitionEnd}
    >
      <div
        className="modal-background"
        onClick={offClick ? handleClose : null}
      />
      <div className="modal">{children}</div>
    </div>
  );
};

export default Modal;

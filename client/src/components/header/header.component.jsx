import React from "react";
import { withRouter } from "react-router-dom";

import "./header.styles.scss";

const Header = ({ history }) => {
  const addItem = () => {
    history.push("/new");
  };
  return (
    <header className="page-header">
      <h1 className="logo">Eye Log</h1>
      <button onClick={addItem}>
        <i className="fas fa-plus" title="Close" />
      </button>
    </header>
  );
};

export default withRouter(Header);

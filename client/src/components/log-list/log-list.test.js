import React from "react";
import { shallow } from "enzyme";
import LogList from "./log-list.component.jsx";

describe("LogList component", () => {
  let wrapper;
  let mockLogData = [
    {
      id: 200,
      timestamp: 1585725735364,
      leftEye: 212,
      rightEye: 213,
      condition: "rested",
      note: "Lorem"
    }
  ];
  let mockHistory = { push: jest.fn() };
  let mockDeleteItem = jest.fn();
  let mockUpdateItem = jest.fn();
  let mockCreateItem = jest.fn();
  let mockProps;

  beforeEach(() => {
    mockProps = {
      logData: mockLogData,
      deleteItem: mockDeleteItem,
      updateItem: mockUpdateItem,
      createItem: mockCreateItem,
      history: mockHistory
    };
    wrapper = shallow(<LogList {...mockProps} />);
  });

  it("should render LogList component", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});

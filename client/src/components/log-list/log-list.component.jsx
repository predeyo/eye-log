import React, { useState, useEffect } from "react";
import { Route, Switch, withRouter } from "react-router-dom";

import LogListItem from "../log-list-item/log-list-item.component.jsx";
import LogItemDetail from "../log-item-detail/log-item-detail.component.jsx";
import LogItemEdit from "../log-item-edit/log-item-edit.component.jsx";

import {
  getLogData,
  updateLogItem,
  createLogItem,
  deleteLogItem
} from "../../utils/api/utils.api.js";

import "./log-list.styles.scss";

const LogList = ({ history }) => {
  const [logData, setLogData] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const data = await getLogData();
      if (data !== null) setLogData(data);
    };
    getData();
  }, [setLogData]);

  const updateItem = async item => {
    const data = await updateLogItem(item);
    if (data !== null) {
      setLogData(data.logData);
      return data.item;
    }
    return null;
  };

  const createItem = async item => {
    const data = await createLogItem(item);
    if (data !== null) {
      setLogData(data.logData);
      return data.item;
    }
    return null;
  };

  const deleteItem = async item => {
    const data = await deleteLogItem(item);
    if (data !== null) {
      setLogData(data);
      return true;
    }
    return null;
  };

  const handleItemSelect = item => {
    history.push(`/${item.id}`);
  };

  return (
    <div className="log-list">
      {logData.map(item => (
        <LogListItem
          key={item.id}
          logItem={item}
          handleItemSelect={handleItemSelect}
        />
      ))}
      <Switch>
        <Route
          path="/new"
          render={props => (
            <LogItemEdit
              {...props}
              logData={logData}
              submitAction={createItem}
            />
          )}
        />
        <Route
          path="/:logId"
          render={props => (
            <LogItemDetail
              {...props}
              logData={logData}
              deleteItem={deleteItem}
            />
          )}
        />
      </Switch>
      <Route
        path="/:logId/edit"
        render={props => (
          <LogItemEdit {...props} logData={logData} submitAction={updateItem} />
        )}
      />
    </div>
  );
};

export default withRouter(LogList);

import React from "react";
import { shallow } from "enzyme";
import LogItemDetail from "./log-item-detail.component.jsx";

describe("LogItemDetail component", () => {
  let wrapper;
  let mockMatch = { params: 200 };
  let mockLogData = [
    {
      id: 200,
      timestamp: 1585725735364,
      leftEye: 212,
      rightEye: 213,
      condition: "rested",
      note: "Lorem"
    }
  ];
  let mockHistory = { push: jest.fn() };
  let mockDeleteItem = jest.fn();
  let mockProps;

  beforeEach(() => {
    mockProps = {
      match: mockMatch,
      logData: mockLogData,
      deleteItem: mockDeleteItem,
      history: mockHistory
    };
    wrapper = shallow(<LogItemDetail {...mockProps} />);
  });

  it("should render LogItemDetail component", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});

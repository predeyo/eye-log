import React, { useEffect, useState } from "react";
import "./log-item-detail.styles.scss";
import { ReactComponent as LeftEyeImg } from "../../assets/left-eye.svg";
import { timestampToDateTime } from "../../utils/utils.js";
import Modal from "../modal/modal.component.jsx";

import { Link } from "react-router-dom";

const LogItemDetail = ({
  match: {
    params: { logId }
  },
  logData,
  deleteItem,
  history
}) => {
  const [logItem, setLogItem] = useState(null);
  const [isModalClosed, setIsModalClosed] = useState(false);

  useEffect(() => {
    setLogItem(logData.find(item => item.id === parseInt(logId)));
  }, [logId, logData]);

  const handleClose = () => {
    setIsModalClosed(!isModalClosed);
    history.push("/");
  };

  const handleDelete = async () => {
    const success = await deleteItem(logItem);
    if (!success) {
      alert("Cannot delete item at this time! Please try again later.");
      return;
    }
    handleClose();
  };

  return (
    <Modal closed={isModalClosed} toggleClosed={handleClose}>
      {logItem ? (
        <div className="log-item-detail">
          <nav>
            <Link to={`/${logId}/edit`}>
              <button>
                <i className="fas fa-pencil-alt" title="Edit" />
              </button>
            </Link>
            <button onClick={handleDelete}>
              <i className="fas fa-trash-alt" title="Delete" />
            </button>
            <button onClick={handleClose}>
              <i className="fas fa-times" title="Close" />
            </button>
          </nav>
          <time>{timestampToDateTime(logItem.timestamp)}</time>
          <div className="eye-detail-container">
            <div className="eye-details">
              <div className="eye-detail">
                <label>Left</label>
                <LeftEyeImg className="eye right-eye" transform="scale(-1,1)" />
                <span>{logItem.leftEye} cm</span>
              </div>
              <div className="eye-detail">
                <label>Right</label>
                <LeftEyeImg className="eye" />
                <span>{logItem.rightEye} cm</span>
              </div>
            </div>
            <div className="condition-container">
              <label>Condition</label>
              <span>{logItem.condition}</span>
            </div>
          </div>
          {logItem.note ? (
            <div className="note">
              <label>Note</label>
              <p>{logItem.note}</p>
            </div>
          ) : null}
        </div>
      ) : (
        <span>Log item not found!</span>
      )}
    </Modal>
  );
};

export default LogItemDetail;

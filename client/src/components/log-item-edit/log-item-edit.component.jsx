import React, { useState, useEffect } from "react";
import "./log-item-edit.styles.scss";
import { ReactComponent as LeftEyeImg } from "../../assets/left-eye.svg";
import { timestampToDateTime } from "../../utils/utils.js";
import Modal from "../modal/modal.component.jsx";

const LogItemEdit = ({
  match: {
    params: { logId }
  },
  logData,
  history,
  submitAction
}) => {
  const [logItem, setLogItem] = useState({
    id: null,
    timestamp: Date.now(),
    rightEye: "",
    leftEye: "",
    condition: "none",
    note: ""
  });
  const [isModalClosed, setIsModalClosed] = useState(false);

  useEffect(() => {
    let existingLogItem = logData.find(item => item.id === parseInt(logId));
    if (existingLogItem) {
      setLogItem({
        id: existingLogItem.id ? existingLogItem.id : null,
        timestamp: existingLogItem.timestamp
          ? existingLogItem.timestamp
          : Date.now(),
        rightEye: existingLogItem.rightEye ? existingLogItem.rightEye : "",
        leftEye: existingLogItem.leftEye ? existingLogItem.leftEye : "",
        condition: existingLogItem.condition
          ? existingLogItem.condition
          : "none",
        note: existingLogItem.note ? existingLogItem.note : ""
      });
    }
  }, [logId, logData]);

  const handleClose = newId => {
    setIsModalClosed(!isModalClosed);
    history.push(`/${logId ? logId : newId || ""}`);
  };
  const handleChange = event => {
    event.preventDefault();
    let name = event.target.name;
    let value = event.target.value;
    setLogItem(prevState => {
      return {
        ...prevState,
        [name]: value
      };
    });
  };

  const handleSubmit = async event => {
    event.preventDefault();
    const item = await submitAction(logItem);
    if (!item) {
      alert("Issues submitting the log item! Please try again later");
      return;
    }
    handleClose(item.id);
  };

  return (
    <Modal
      closed={isModalClosed}
      toggleClosed={() => handleClose(null)}
      offClick={true}
    >
      <div className="log-item-edit">
        <form onSubmit={handleSubmit}>
          <nav>
            <button type="submit">
              <i className="fas fa-check" title="Submit" />
            </button>
            <button onClick={() => handleClose(null)}>
              <i className="fas fa-times" title="Close" />
            </button>
          </nav>
          <time>{timestampToDateTime(logItem.timestamp)}</time>
          <div className="eye-detail-container">
            <div className="eye-details">
              <div className="eye-detail">
                <label>Left</label>
                <LeftEyeImg className="eye" transform="scale(-1,1)" />
                <span>
                  <input
                    name="leftEye"
                    type="number"
                    placeholder="Focal plane distance"
                    min="1"
                    max="1000000"
                    value={logItem.leftEye}
                    onChange={handleChange}
                    required
                  />
                  cm
                </span>
              </div>
              <div className="eye-detail">
                <label>Right</label>
                <LeftEyeImg className="eye" />
                <span>
                  <input
                    name="rightEye"
                    type="number"
                    placeholder="Focal plane distance"
                    min="1"
                    max="1000000"
                    value={logItem.rightEye}
                    onChange={handleChange}
                    required
                  />{" "}
                  cm
                </span>
              </div>
            </div>
            <div className="condition-container">
              <label>Condition</label>
              <select
                name="condition"
                value={logItem.condition}
                onChange={handleChange}
              >
                <option value="strained">Strained</option>
                <option value="none">None</option>
                <option value="rested">Rested</option>
              </select>
            </div>
          </div>
          <label>Note</label>
          <textarea
            name="note"
            placeholder="Something to note?"
            cols="30"
            rows="10"
            maxLength="600"
            value={logItem.note}
            onChange={handleChange}
          ></textarea>
        </form>
      </div>
    </Modal>
  );
};

export default LogItemEdit;

const LOREM_IPSUM = ` Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit.
cursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at pretium tortor accumsan et.`;

const CONDITION = ["rested", "strained", "none"];

// max - not included as maximums value
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

export const generateLogData = amount => {
  let data = [];
  for (var i = 0; i < amount; i++) {
    data.push({
      id: amount - i,
      timestamp: Date.now() - i * 86400 * 1000,
      leftEye: 12 + amount - i + getRandomInt(3),
      rightEye: 12 + amount - i + getRandomInt(3),
      condition: CONDITION[getRandomInt(3)],
      note: LOREM_IPSUM.slice(0, getRandomInt(LOREM_IPSUM.length))
    });
  }
  return data;
};

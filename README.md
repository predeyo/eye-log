# Eye Log

Application to help you keep track of ones myopia, by storing focal plane distance and condition relative to time.

Project created with React.js bootstrapped with Create React App.

## Available Scripts

In the project directory, you can run:

### `npm run install-all`

Install required node modules for server and client

### `npm run dev`

Runs server and client for development.

### `npm run test`

Launches the test runner.

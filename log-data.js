const LOG_DATA = [
  {
    id: 200,
    timestamp: 1585725735364,
    leftEye: 212,
    rightEye: 213,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabi"
  },
  {
    id: 199,
    timestamp: 1585639335364,
    leftEye: 212,
    rightEye: 211,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis"
  },
  {
    id: 198,
    timestamp: 1585552935364,
    leftEye: 212,
    rightEye: 212,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget "
  },
  {
    id: 197,
    timestamp: 1585466535364,
    leftEye: 210,
    rightEye: 210,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lob"
  },
  {
    id: 196,
    timestamp: 1585380135364,
    leftEye: 209,
    rightEye: 209,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravi"
  },
  {
    id: 195,
    timestamp: 1585293735364,
    leftEye: 209,
    rightEye: 209,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gr"
  },
  {
    id: 194,
    timestamp: 1585207335365,
    leftEye: 208,
    rightEye: 207,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vest"
  },
  {
    id: 193,
    timestamp: 1585120935365,
    leftEye: 206,
    rightEye: 206,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed bla"
  },
  {
    id: 192,
    timestamp: 1585034535365,
    leftEye: 206,
    rightEye: 204,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Se"
  },
  {
    id: 191,
    timestamp: 1584948135365,
    leftEye: 205,
    rightEye: 205,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque "
  },
  {
    id: 190,
    timestamp: 1584861735365,
    leftEye: 204,
    rightEye: 203,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehic"
  },
  {
    id: 189,
    timestamp: 1584775335365,
    leftEye: 202,
    rightEye: 202,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maxim"
  },
  {
    id: 188,
    timestamp: 1584688935365,
    leftEye: 200,
    rightEye: 201,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices e"
  },
  {
    id: 187,
    timestamp: 1584602535365,
    leftEye: 199,
    rightEye: 201,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices element"
  },
  {
    id: 186,
    timestamp: 1584516135365,
    leftEye: 198,
    rightEye: 198,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Mor"
  },
  {
    id: 185,
    timestamp: 1584429735365,
    leftEye: 197,
    rightEye: 197,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna n"
  },
  {
    id: 184,
    timestamp: 1584343335365,
    leftEye: 198,
    rightEye: 197,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet"
  },
  {
    id: 183,
    timestamp: 1584256935365,
    leftEye: 197,
    rightEye: 195,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna"
  },
  {
    id: 182,
    timestamp: 1584170535365,
    leftEye: 195,
    rightEye: 196,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lob"
  },
  {
    id: 181,
    timestamp: 1584084135365,
    leftEye: 193,
    rightEye: 195,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifen"
  },
  {
    id: 180,
    timestamp: 1583997735365,
    leftEye: 193,
    rightEye: 193,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementu"
  },
  {
    id: 179,
    timestamp: 1583911335365,
    leftEye: 192,
    rightEye: 191,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lo"
  },
  {
    id: 178,
    timestamp: 1583824935365,
    leftEye: 192,
    rightEye: 190,
    condition: "strained",
    note: " Lorem ipsum dolor sit amet, conse"
  },
  {
    id: 177,
    timestamp: 1583738535365,
    leftEye: 191,
    rightEye: 190,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibul"
  },
  {
    id: 176,
    timestamp: 1583652135365,
    leftEye: 190,
    rightEye: 190,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet moll"
  },
  {
    id: 175,
    timestamp: 1583565735365,
    leftEye: 189,
    rightEye: 187,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lob"
  },
  {
    id: 174,
    timestamp: 1583479335365,
    leftEye: 188,
    rightEye: 186,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec bland"
  },
  {
    id: 173,
    timestamp: 1583392935365,
    leftEye: 187,
    rightEye: 186,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget"
  },
  {
    id: 172,
    timestamp: 1583306535365,
    leftEye: 185,
    rightEye: 185,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursu"
  },
  {
    id: 171,
    timestamp: 1583220135365,
    leftEye: 184,
    rightEye: 184,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vest"
  },
  {
    id: 170,
    timestamp: 1583133735365,
    leftEye: 183,
    rightEye: 184,
    condition: "strained",
    note: " Lorem ipsum dolor sit a"
  },
  {
    id: 169,
    timestamp: 1583047335365,
    leftEye: 181,
    rightEye: 182,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laor"
  },
  {
    id: 168,
    timestamp: 1582960935365,
    leftEye: 180,
    rightEye: 180,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibu"
  },
  {
    id: 167,
    timestamp: 1582874535365,
    leftEye: 179,
    rightEye: 181,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus"
  },
  {
    id: 166,
    timestamp: 1582788135365,
    leftEye: 178,
    rightEye: 180,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. "
  },
  {
    id: 165,
    timestamp: 1582701735365,
    leftEye: 178,
    rightEye: 178,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit"
  },
  {
    id: 164,
    timestamp: 1582615335365,
    leftEye: 177,
    rightEye: 176,
    condition: "strained",
    note: " Lorem ip"
  },
  {
    id: 163,
    timestamp: 1582528935365,
    leftEye: 176,
    rightEye: 176,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pu"
  },
  {
    id: 162,
    timestamp: 1582442535365,
    leftEye: 175,
    rightEye: 174,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id p"
  },
  {
    id: 161,
    timestamp: 1582356135365,
    leftEye: 174,
    rightEye: 175,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvin"
  },
  {
    id: 160,
    timestamp: 1582269735365,
    leftEye: 173,
    rightEye: 174,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligu"
  },
  {
    id: 159,
    timestamp: 1582183335365,
    leftEye: 173,
    rightEye: 172,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas."
  },
  {
    id: 158,
    timestamp: 1582096935365,
    leftEye: 171,
    rightEye: 170,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit c"
  },
  {
    id: 157,
    timestamp: 1582010535365,
    leftEye: 170,
    rightEye: 169,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices"
  },
  {
    id: 156,
    timestamp: 1581924135365,
    leftEye: 169,
    rightEye: 168,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at pretium tortor accu"
  },
  {
    id: 155,
    timestamp: 1581837735365,
    leftEye: 169,
    rightEye: 167,
    condition: "none",
    note: " Lorem ipsum dolor sit am"
  },
  {
    id: 154,
    timestamp: 1581751335365,
    leftEye: 166,
    rightEye: 166,
    condition: "none",
    note: " Lorem ipsum dolor sit amet, consectetur adipisc"
  },
  {
    id: 153,
    timestamp: 1581664935365,
    leftEye: 165,
    rightEye: 166,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Pr"
  },
  {
    id: 152,
    timestamp: 1581578535365,
    leftEye: 164,
    rightEye: 164,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis se"
  },
  {
    id: 151,
    timestamp: 1581492135365,
    leftEye: 163,
    rightEye: 165,
    condition: "none",
    note: " Lorem ipsum"
  },
  {
    id: 150,
    timestamp: 1581405735365,
    leftEye: 164,
    rightEye: 162,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non"
  },
  {
    id: 149,
    timestamp: 1581319335365,
    leftEye: 161,
    rightEye: 161,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex comm"
  },
  {
    id: 148,
    timestamp: 1581232935365,
    leftEye: 161,
    rightEye: 162,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales puru"
  },
  {
    id: 147,
    timestamp: 1581146535365,
    leftEye: 161,
    rightEye: 160,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravid"
  },
  {
    id: 146,
    timestamp: 1581060135365,
    leftEye: 160,
    rightEye: 158,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , s"
  },
  {
    id: 145,
    timestamp: 1580973735365,
    leftEye: 157,
    rightEye: 158,
    condition: "none",
    note: " Lorem ipsum dolor sit amet, consect"
  },
  {
    id: 144,
    timestamp: 1580887335365,
    leftEye: 158,
    rightEye: 158,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce grav"
  },
  {
    id: 143,
    timestamp: 1580800935365,
    leftEye: 156,
    rightEye: 155,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulv"
  },
  {
    id: 142,
    timestamp: 1580714535365,
    leftEye: 156,
    rightEye: 154,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non "
  },
  {
    id: 141,
    timestamp: 1580628135365,
    leftEye: 155,
    rightEye: 154,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at preti"
  },
  {
    id: 140,
    timestamp: 1580541735365,
    leftEye: 153,
    rightEye: 152,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id p"
  },
  {
    id: 139,
    timestamp: 1580455335365,
    leftEye: 151,
    rightEye: 152,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida"
  },
  {
    id: 138,
    timestamp: 1580368935365,
    leftEye: 151,
    rightEye: 150,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit"
  },
  {
    id: 137,
    timestamp: 1580282535365,
    leftEye: 149,
    rightEye: 150,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at pretium tortor accu"
  },
  {
    id: 136,
    timestamp: 1580196135365,
    leftEye: 148,
    rightEye: 150,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula"
  },
  {
    id: 135,
    timestamp: 1580109735365,
    leftEye: 148,
    rightEye: 147,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet "
  },
  {
    id: 134,
    timestamp: 1580023335365,
    leftEye: 148,
    rightEye: 148,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Mo"
  },
  {
    id: 133,
    timestamp: 1579936935365,
    leftEye: 146,
    rightEye: 147,
    condition: "strained",
    note: " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque"
  },
  {
    id: 132,
    timestamp: 1579850535365,
    leftEye: 144,
    rightEye: 145,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex com"
  },
  {
    id: 131,
    timestamp: 1579764135365,
    leftEye: 144,
    rightEye: 143,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultri"
  },
  {
    id: 130,
    timestamp: 1579677735365,
    leftEye: 143,
    rightEye: 143,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turp"
  },
  {
    id: 129,
    timestamp: 1579591335365,
    leftEye: 142,
    rightEye: 142,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna"
  },
  {
    id: 128,
    timestamp: 1579504935365,
    leftEye: 140,
    rightEye: 141,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendreri"
  },
  {
    id: 127,
    timestamp: 1579418535365,
    leftEye: 140,
    rightEye: 141,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabi"
  },
  {
    id: 126,
    timestamp: 1579332135365,
    leftEye: 138,
    rightEye: 138,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit"
  },
  {
    id: 125,
    timestamp: 1579245735365,
    leftEye: 138,
    rightEye: 139,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis q"
  },
  {
    id: 124,
    timestamp: 1579159335365,
    leftEye: 137,
    rightEye: 137,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commo"
  },
  {
    id: 123,
    timestamp: 1579072935365,
    leftEye: 136,
    rightEye: 137,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed e"
  },
  {
    id: 122,
    timestamp: 1578986535365,
    leftEye: 134,
    rightEye: 136,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur fel"
  },
  {
    id: 121,
    timestamp: 1578900135365,
    leftEye: 135,
    rightEye: 133,
    condition: "none",
    note: " Lorem ipsum dolor sit amet, consectetur "
  },
  {
    id: 120,
    timestamp: 1578813735365,
    leftEye: 133,
    rightEye: 133,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar."
  },
  {
    id: 119,
    timestamp: 1578727335365,
    leftEye: 131,
    rightEye: 132,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Du"
  },
  {
    id: 118,
    timestamp: 1578640935365,
    leftEye: 131,
    rightEye: 131,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet a"
  },
  {
    id: 117,
    timestamp: 1578554535365,
    leftEye: 130,
    rightEye: 130,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a "
  },
  {
    id: 116,
    timestamp: 1578468135365,
    leftEye: 129,
    rightEye: 130,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blan"
  },
  {
    id: 115,
    timestamp: 1578381735365,
    leftEye: 129,
    rightEye: 129,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales "
  },
  {
    id: 114,
    timestamp: 1578295335365,
    leftEye: 126,
    rightEye: 128,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicu"
  },
  {
    id: 113,
    timestamp: 1578208935365,
    leftEye: 125,
    rightEye: 126,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit "
  },
  {
    id: 112,
    timestamp: 1578122535365,
    leftEye: 125,
    rightEye: 124,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at pretium tortor accumsan et"
  },
  {
    id: 111,
    timestamp: 1578036135365,
    leftEye: 125,
    rightEye: 124,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultr"
  },
  {
    id: 110,
    timestamp: 1577949735365,
    leftEye: 123,
    rightEye: 122,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a"
  },
  {
    id: 109,
    timestamp: 1577863335365,
    leftEye: 122,
    rightEye: 122,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turp"
  },
  {
    id: 108,
    timestamp: 1577776935365,
    leftEye: 122,
    rightEye: 121,
    condition: "rested",
    note: " Lorem "
  },
  {
    id: 107,
    timestamp: 1577690535365,
    leftEye: 120,
    rightEye: 121,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum"
  },
  {
    id: 106,
    timestamp: 1577604135365,
    leftEye: 119,
    rightEye: 118,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a ni"
  },
  {
    id: 105,
    timestamp: 1577517735365,
    leftEye: 118,
    rightEye: 119,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncurs"
  },
  {
    id: 104,
    timestamp: 1577431335365,
    leftEye: 118,
    rightEye: 117,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices e"
  },
  {
    id: 103,
    timestamp: 1577344935365,
    leftEye: 116,
    rightEye: 116,
    condition: "none",
    note: " Lorem ipsum do"
  },
  {
    id: 102,
    timestamp: 1577258535365,
    leftEye: 116,
    rightEye: 114,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ul"
  },
  {
    id: 101,
    timestamp: 1577172135365,
    leftEye: 115,
    rightEye: 114,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Ves"
  },
  {
    id: 100,
    timestamp: 1577085735365,
    leftEye: 114,
    rightEye: 113,
    condition: "rested",
    note: " Lorem ipsum dolor sit amet, consectetur adipiscin"
  },
  {
    id: 99,
    timestamp: 1576999335365,
    leftEye: 111,
    rightEye: 113,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. D"
  },
  {
    id: 98,
    timestamp: 1576912935365,
    leftEye: 111,
    rightEye: 110,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvi"
  },
  {
    id: 97,
    timestamp: 1576826535365,
    leftEye: 111,
    rightEye: 109,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet"
  },
  {
    id: 96,
    timestamp: 1576740135365,
    leftEye: 109,
    rightEye: 109,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla "
  },
  {
    id: 95,
    timestamp: 1576653735365,
    leftEye: 108,
    rightEye: 109,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at pretium tortor accum"
  },
  {
    id: 94,
    timestamp: 1576567335365,
    leftEye: 106,
    rightEye: 106,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis h"
  },
  {
    id: 93,
    timestamp: 1576480935365,
    leftEye: 105,
    rightEye: 107,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id p"
  },
  {
    id: 92,
    timestamp: 1576394535365,
    leftEye: 106,
    rightEye: 105,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpi"
  },
  {
    id: 91,
    timestamp: 1576308135365,
    leftEye: 103,
    rightEye: 103,
    condition: "none",
    note: " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisq"
  },
  {
    id: 90,
    timestamp: 1576221735365,
    leftEye: 103,
    rightEye: 102,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur fel"
  },
  {
    id: 89,
    timestamp: 1576135335365,
    leftEye: 101,
    rightEye: 102,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec b"
  },
  {
    id: 88,
    timestamp: 1576048935365,
    leftEye: 101,
    rightEye: 100,
    condition: "none",
    note: " Lorem ipsum dol"
  },
  {
    id: 87,
    timestamp: 1575962535365,
    leftEye: 101,
    rightEye: 99,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo ele"
  },
  {
    id: 86,
    timestamp: 1575876135365,
    leftEye: 98,
    rightEye: 98,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at"
  },
  {
    id: 85,
    timestamp: 1575789735365,
    leftEye: 98,
    rightEye: 98,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur f"
  },
  {
    id: 84,
    timestamp: 1575703335365,
    leftEye: 98,
    rightEye: 97,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non"
  },
  {
    id: 83,
    timestamp: 1575616935365,
    leftEye: 95,
    rightEye: 95,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis h"
  },
  {
    id: 82,
    timestamp: 1575530535365,
    leftEye: 96,
    rightEye: 96,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo e"
  },
  {
    id: 81,
    timestamp: 1575444135365,
    leftEye: 93,
    rightEye: 93,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum"
  },
  {
    id: 80,
    timestamp: 1575357735365,
    leftEye: 93,
    rightEye: 94,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at pretium to"
  },
  {
    id: 79,
    timestamp: 1575271335365,
    leftEye: 92,
    rightEye: 92,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , "
  },
  {
    id: 78,
    timestamp: 1575184935365,
    leftEye: 90,
    rightEye: 92,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit am"
  },
  {
    id: 77,
    timestamp: 1575098535365,
    leftEye: 90,
    rightEye: 90,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulv"
  },
  {
    id: 76,
    timestamp: 1575012135365,
    leftEye: 88,
    rightEye: 89,
    condition: "rested",
    note: " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque "
  },
  {
    id: 75,
    timestamp: 1574925735365,
    leftEye: 89,
    rightEye: 87,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla faci"
  },
  {
    id: 74,
    timestamp: 1574839335365,
    leftEye: 86,
    rightEye: 86,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pul"
  },
  {
    id: 73,
    timestamp: 1574752935365,
    leftEye: 87,
    rightEye: 86,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at f"
  },
  {
    id: 72,
    timestamp: 1574666535365,
    leftEye: 84,
    rightEye: 85,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hen"
  },
  {
    id: 71,
    timestamp: 1574580135365,
    leftEye: 85,
    rightEye: 85,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. "
  },
  {
    id: 70,
    timestamp: 1574493735365,
    leftEye: 82,
    rightEye: 82,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at f"
  },
  {
    id: 69,
    timestamp: 1574407335365,
    leftEye: 82,
    rightEye: 81,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoree"
  },
  {
    id: 68,
    timestamp: 1574320935365,
    leftEye: 82,
    rightEye: 80,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Cura"
  },
  {
    id: 67,
    timestamp: 1574234535365,
    leftEye: 80,
    rightEye: 80,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, co"
  },
  {
    id: 66,
    timestamp: 1574148135365,
    leftEye: 79,
    rightEye: 78,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morb"
  },
  {
    id: 65,
    timestamp: 1574061735365,
    leftEye: 77,
    rightEye: 79,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur max"
  },
  {
    id: 64,
    timestamp: 1573975335365,
    leftEye: 78,
    rightEye: 76,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum al"
  },
  {
    id: 63,
    timestamp: 1573888935365,
    leftEye: 76,
    rightEye: 75,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus"
  },
  {
    id: 62,
    timestamp: 1573802535365,
    leftEye: 75,
    rightEye: 74,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed bland"
  },
  {
    id: 61,
    timestamp: 1573716135365,
    leftEye: 74,
    rightEye: 73,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ul"
  },
  {
    id: 60,
    timestamp: 1573629735365,
    leftEye: 72,
    rightEye: 72,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulu"
  },
  {
    id: 59,
    timestamp: 1573543335365,
    leftEye: 72,
    rightEye: 71,
    condition: "strained",
    note: " Lorem ipsum dolor sit amet, cons"
  },
  {
    id: 58,
    timestamp: 1573456935365,
    leftEye: 72,
    rightEye: 70,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet li"
  },
  {
    id: 57,
    timestamp: 1573370535365,
    leftEye: 69,
    rightEye: 69,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis ,"
  },
  {
    id: 56,
    timestamp: 1573284135365,
    leftEye: 68,
    rightEye: 68,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum"
  },
  {
    id: 55,
    timestamp: 1573197735365,
    leftEye: 68,
    rightEye: 68,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus"
  },
  {
    id: 54,
    timestamp: 1573111335365,
    leftEye: 67,
    rightEye: 68,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula ve"
  },
  {
    id: 53,
    timestamp: 1573024935365,
    leftEye: 65,
    rightEye: 67,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultric"
  },
  {
    id: 52,
    timestamp: 1572938535365,
    leftEye: 66,
    rightEye: 66,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellent"
  },
  {
    id: 51,
    timestamp: 1572852135365,
    leftEye: 63,
    rightEye: 64,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turp"
  },
  {
    id: 50,
    timestamp: 1572765735365,
    leftEye: 63,
    rightEye: 62,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit am"
  },
  {
    id: 49,
    timestamp: 1572679335365,
    leftEye: 61,
    rightEye: 61,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blan"
  },
  {
    id: 48,
    timestamp: 1572592935365,
    leftEye: 60,
    rightEye: 60,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo e"
  },
  {
    id: 47,
    timestamp: 1572506535365,
    leftEye: 61,
    rightEye: 61,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet tur"
  },
  {
    id: 46,
    timestamp: 1572420135365,
    leftEye: 58,
    rightEye: 60,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis au"
  },
  {
    id: 45,
    timestamp: 1572333735365,
    leftEye: 59,
    rightEye: 57,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellen"
  },
  {
    id: 44,
    timestamp: 1572247335365,
    leftEye: 56,
    rightEye: 57,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam"
  },
  {
    id: 43,
    timestamp: 1572160935365,
    leftEye: 57,
    rightEye: 57,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce grav"
  },
  {
    id: 42,
    timestamp: 1572074535365,
    leftEye: 54,
    rightEye: 56,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Null"
  },
  {
    id: 41,
    timestamp: 1571988135365,
    leftEye: 53,
    rightEye: 55,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum a"
  },
  {
    id: 40,
    timestamp: 1571901735365,
    leftEye: 54,
    rightEye: 54,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit c"
  },
  {
    id: 39,
    timestamp: 1571815335365,
    leftEye: 51,
    rightEye: 52,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent effi"
  },
  {
    id: 38,
    timestamp: 1571728935365,
    leftEye: 52,
    rightEye: 51,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulu"
  },
  {
    id: 37,
    timestamp: 1571642535365,
    leftEye: 50,
    rightEye: 50,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id"
  },
  {
    id: 36,
    timestamp: 1571556135365,
    leftEye: 48,
    rightEye: 50,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed"
  },
  {
    id: 35,
    timestamp: 1571469735365,
    leftEye: 47,
    rightEye: 47,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula "
  },
  {
    id: 34,
    timestamp: 1571383335365,
    leftEye: 46,
    rightEye: 46,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at pre"
  },
  {
    id: 33,
    timestamp: 1571296935365,
    leftEye: 47,
    rightEye: 46,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehi"
  },
  {
    id: 32,
    timestamp: 1571210535365,
    leftEye: 44,
    rightEye: 46,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices "
  },
  {
    id: 31,
    timestamp: 1571124135365,
    leftEye: 45,
    rightEye: 43,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at pretium tor"
  },
  {
    id: 30,
    timestamp: 1571037735365,
    leftEye: 42,
    rightEye: 43,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at pretium "
  },
  {
    id: 29,
    timestamp: 1570951335365,
    leftEye: 42,
    rightEye: 43,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condi"
  },
  {
    id: 28,
    timestamp: 1570864935365,
    leftEye: 42,
    rightEye: 40,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvi"
  },
  {
    id: 27,
    timestamp: 1570778535365,
    leftEye: 39,
    rightEye: 39,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula"
  },
  {
    id: 26,
    timestamp: 1570692135365,
    leftEye: 39,
    rightEye: 39,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices e"
  },
  {
    id: 25,
    timestamp: 1570605735365,
    leftEye: 39,
    rightEye: 37,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex comm"
  },
  {
    id: 24,
    timestamp: 1570519335365,
    leftEye: 38,
    rightEye: 36,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, at pretium tortor accumsan"
  },
  {
    id: 23,
    timestamp: 1570432935365,
    leftEye: 36,
    rightEye: 36,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusc"
  },
  {
    id: 22,
    timestamp: 1570346535365,
    leftEye: 36,
    rightEye: 36,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl "
  },
  {
    id: 21,
    timestamp: 1570260135365,
    leftEye: 34,
    rightEye: 34,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. F"
  },
  {
    id: 20,
    timestamp: 1570173735365,
    leftEye: 34,
    rightEye: 34,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, con"
  },
  {
    id: 19,
    timestamp: 1570087335365,
    leftEye: 31,
    rightEye: 33,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n"
  },
  {
    id: 18,
    timestamp: 1570000935365,
    leftEye: 31,
    rightEye: 31,
    condition: "none",
    note: " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Q"
  },
  {
    id: 17,
    timestamp: 1569914535365,
    leftEye: 31,
    rightEye: 31,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam "
  },
  {
    id: 16,
    timestamp: 1569828135365,
    leftEye: 28,
    rightEye: 28,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed "
  },
  {
    id: 15,
    timestamp: 1569741735365,
    leftEye: 28,
    rightEye: 29,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna"
  },
  {
    id: 14,
    timestamp: 1569655335365,
    leftEye: 26,
    rightEye: 28,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices ege"
  },
  {
    id: 13,
    timestamp: 1569568935365,
    leftEye: 27,
    rightEye: 27,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non"
  },
  {
    id: 12,
    timestamp: 1569482535365,
    leftEye: 26,
    rightEye: 24,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elementum felis, a"
  },
  {
    id: 11,
    timestamp: 1569396135365,
    leftEye: 23,
    rightEye: 23,
    condition: "rested",
    note: " Lorem ipsum dolor sit amet"
  },
  {
    id: 10,
    timestamp: 1569309735365,
    leftEye: 22,
    rightEye: 24,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo"
  },
  {
    id: 9,
    timestamp: 1569223335365,
    leftEye: 23,
    rightEye: 23,
    condition: "none",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. Nulla facilisi. Duis eleifend magna nec blandit cursus. Morbi non velit a nisl ultrices egestas. Vestibulum ultrices elemen"
  },
  {
    id: 8,
    timestamp: 1569136935365,
    leftEye: 22,
    rightEye: 22,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum aliquet ante. N"
  },
  {
    id: 7,
    timestamp: 1569050535365,
    leftEye: 20,
    rightEye: 19,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus,"
  },
  {
    id: 6,
    timestamp: 1568964135365,
    leftEye: 19,
    rightEye: 19,
    condition: "rested",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, s"
  },
  {
    id: 5,
    timestamp: 1568877735365,
    leftEye: 19,
    rightEye: 19,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendr"
  },
  {
    id: 4,
    timestamp: 1568791335365,
    leftEye: 17,
    rightEye: 17,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at"
  },
  {
    id: 3,
    timestamp: 1568704935365,
    leftEye: 15,
    rightEye: 17,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo eleifend. Curabitur maximus ultrices quam id pulvinar. Praesent efficitur felis sit amet ligula vehicula lobortis. Fusce gravida leo at felis , sed blandit turpis hendrerit. \n\ncursus, condimentum a"
  },
  {
    id: 2,
    timestamp: 1568618535365,
    leftEye: 14,
    rightEye: 14,
    condition: "rested",
    note: ""
  },
  {
    id: 1,
    timestamp: 1568532135365,
    leftEye: 14,
    rightEye: 15,
    condition: "strained",
    note:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet turpis quis augue laoreet mollis pellentesque non sem. Sed eget sodales purus, sed vestibulum est. Nulla ut turpis sed ex commodo "
  }
];

module.exports = LOG_DATA;
